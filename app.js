const express = require("express");
const app = express();
const todoRoutes = require("./routes/todo.js");

app.use(express.json());

app.use("/api/todo", todoRoutes);

module.exports = app;
