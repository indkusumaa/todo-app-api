const { Todo } = require("../models");
const Validator = require("fastest-validator");
const v = new Validator();

//--CHECK IF EXIST
const checkIfExist = async (req, res, next) => {
  const id = req.params.id;
  try {
    const data = await Todo.findOne({
      where: {
        id: id,
        isDeleted: false,
      },
    });
    return data;
  } catch (error) {
    console.log(error);
  }
};

//--CREATE TODO
const setTodo = async (req, res, next) => {
  const data = {
    title: req.body.title,
    isDeleted: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  console.log(data);
  //SCHEMA
  const schema = {
    title: { type: "string", min: 5, max: 50, optional: false },
  };

  const validationResult = v.validate(data, schema);
  if (validationResult !== true) {
    //data not valid
    res.status(400).json({
      Error: validationResult,
    });
  } else {
    // data valid ready to insert
    try {
      const result = await Todo.create(data);
      res.status(201).json({
        message: "success",
        data: result,
      });
    } catch (error) {
      res.status(500).json({
        message: "failed",
        Error: error,
      });
    }
  }
};

//-- GET ALL TODO
const getTodoList = async (req, res, next) => {
  try {
    const data = await Todo.findAll({
      where: {
        isDeleted: false,
      },
    });
    res.status(200).json({
      message: "success",
      data: data,
    });
  } catch (error) {
    res.status(400).json({
      message: "failed",
      Error: error,
    });
  }
};

//-- GET DETAIL TODO
const getTodo = async (req, res, next) => {
  const data_exist = await checkIfExist(req, res, next);
  //console.log(data_exist?.dataValues);
  // check if id exists
  if (data_exist) {
    const id = req.params.id;
    try {
      //if exist find the data
      const data = await Todo.findOne({
        where: {
          id: id,
          isDeleted: false,
        },
      });
      res.status(200).json({
        message: "success",
        data: data,
      });
    } catch (error) {
      res.status(400).json({
        message: "failed",
        Error: error,
      });
    }
  } else {
    res.status(404).json({
      message: "Data Not Found",
    });
  }
};

//-- DELETE TODO (SOFT DELETE)
const deleteTodo = async (req, res, next) => {
  const id = req.params.id;
  const data = {
    isDeleted: true,
    deletedAt: new Date(),
  };

  //check if exist
  const data_exist = await checkIfExist(req, res, next);
  if (data_exist?.dataValues) {
    console.log(data_exist?.dataValues);
    //update data if exist
    try {
      const result = await Todo.update(data, { where: { id: id } });
      res.status(201).json({
        message: "success",
        rows_affected: result,
      });
    } catch (error) {
      res.status(500).json({
        message: "Failed delete user data",
        Error: error,
      });
    }
  } else {
    res.status(400).json({
      message: "Data not Found",
    });
  }
};
module.exports = { getTodoList, getTodo, setTodo, deleteTodo };
