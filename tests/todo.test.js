const app = require("../app");
const request = require("supertest");

describe("GET /api/todo", () => {
  it("should response with a 200 status code", (done) => {
    request(app)
    .get("/api/todo")
    .expect("Content-Type", 'application/json; charset=utf-8')
    .expect(200);
    done()
  });
});

describe("POST /api/todo", () => {
  it("should response with a 201 status code", (done) => {
    request(app)
    .post("/api/todo")
    .set({title:'learn Java'})
    .expect("Content-Type", 'application/json; charset=utf-8')
    .expect(201);
    done()
  });
});

describe("GET /api/todo/:id", () => {
  it("should response with a 200 status code", (done) => {
    request(app)
    .post("/api/todo/")
    .set({title:'learn Java'})
    .expect("Content-Type", 'application/json; charset=utf-8')
    .expect(201);
    
    request(app)
    .get("/api/todo/1")
    .expect("Content-Type", 'application/json; charset=utf-8')
    .expect(200);
    done()
  });
});

describe("DELETE /api/todo/:id", () => {
  it("should response with a 201 status code", (done) => {
    request(app)
    .post("/api/todo/")
    .set({title:'learn Java'})
    .expect("Content-Type", 'application/json; charset=utf-8')
    .expect(201);
    
    request(app)
    .delete("/api/todo/1")
    .expect("Content-Type", 'application/json; charset=utf-8')
    .expect(201);
    done()
  });
});